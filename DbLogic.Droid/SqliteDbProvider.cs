﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Mono.Data.Sqlite;
using Android.Util;
using System.Threading.Tasks;
using App1.DataStorage;
using Xamarin.Forms;

namespace DbLogic.Droid
{
    public class SQLiteDbProvider
    {
        private string _connectionStringWorking, _connectionStringLoging;
        private static object _lockWorking, _lockLoging;
        private SqliteConnection _connectionWorking, _connectionLoging;
        
        public static SQLiteDbProvider Instance { get; private set; }
        
        static SQLiteDbProvider()
        {
            Instance = new SQLiteDbProvider();
        }

        private SQLiteDbProvider()
        {
            _connectionStringLoging = string.Format("Uri = file:{0}; Pooling = true; Max Pool Size = 2", DependencyService.Get<ISqlite>().GetDatabasePath("Logs.db3"));
            _connectionLoging = new SqliteConnection(_connectionStringLoging);
            _lockLoging = new object();

            _connectionStringWorking = string.Format("Uri = file:{0}; Pooling = true; Max Pool Size = 2", DependencyService.Get<ISqlite>().GetDatabasePath("E-Bookkeepper.db3"));
            _connectionWorking = new SqliteConnection(_connectionStringWorking);
            _lockWorking = new object();
        }

        #region Working
        public uint ExecuteNonQuery(string sql)
        {
            uint result = 0;
            lock (_lockWorking)
            {
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    command.CommandText = sql;
                    try
                    {
                        result = (uint)command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Warning", string.Format("Помилка {0} виконання Sql запиту {1}", ex.Message, sql));
                    }
                    finally
                    {
                        _connectionWorking.Close();
                    }
                }
            }
            return result;
        }

        public uint InsertAndGetId(string sql)
        {
            uint result = 0;
            lock (_lockWorking)
            {
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    command.CommandText = sql;
                    try
                    {
                        command.ExecuteNonQuery();
                        command.CommandText = "select last_insert_rowid()";
                        result = Convert.ToUInt32(command.ExecuteScalar().ToString());
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Warning", string.Format("Помилка {0} виконання Sql запиту {1}", ex.Message, sql));
                    }
                    finally
                    {
                        _connectionWorking.Close();
                    }
                }
            }
            return result;
        }

        public void TruncateTable(string table)
        {
            lock (_lockWorking)
            {
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    command.CommandText = "DELETE FROM " + table;
                    try
                    {
                        command.ExecuteNonQuery();
                        command.CommandText = "VACUUM";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Warning", string.Format("Помилка {0} виконання Sql запиту {1}", ex.Message, command.CommandText));
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        _connectionWorking.Close();
                    }
                }
            }
        }

        public object ScalarQuery(string sql)
        {
            lock (_lockWorking)
            {
                object val = null;
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    command.CommandText = sql;
                    try
                    {
                        val = command.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Warning", string.Format("Помилка {0} виконання Sql запиту {1}", ex.Message, sql));
                    }
                    finally
                    {
                        _connectionWorking.Close();
                    }
                }
                if (val is DBNull) return null;
                return val;
            }
        }

        public DataTable GetTable(string sql)
        {
            lock (_lockWorking)
            {
                var table = new DataTable();
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    command.CommandText = sql;
                    table.TableName = "1";
                    var p = new SqliteDataAdapter(command);
                    try
                    {
                        p.Fill(table);
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Warning", string.Format("Помилка {0} заливки даних в таблицю. Запиту {1}", ex.Message, sql));
                    }
                    finally
                    {
                        _connectionWorking.Close();
                    }
                }
                return table;
            }
        }

        public void ClearDB()
        {
            lock (_connectionWorking)
            {
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    command.CommandText = "SELECT name FROM sqlite_master WHERE type='table'";
                    var table = new DataTable();
                    table.TableName = "1";
                    var p = new SqliteDataAdapter(command);
                    try
                    {
                        p.Fill(table);
                        foreach (DataRow row in table.Rows)
                        {
                            command.CommandText = String.Format("DELETE FROM {0}", row[0].ToString());
                            command.ExecuteNonQuery();
                        }
                        command.CommandText = "VACUUM";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Warning", string.Format("Помилка {0} виконання Sql запиту {1}", ex.Message, command.CommandText));
                    }
                    finally
                    {
                        _connectionWorking.Close();
                    }
                }
            }
        }

        public void UpdateDB(string[] sqls)
        {
            if (sqls == null || sqls.Length == 0) return;
            lock (_connectionWorking)
            {
                _connectionWorking.Open();
                using (SqliteCommand command = _connectionWorking.CreateCommand())
                {
                    for (byte i = 0; i < sqls.Length; i++)
                    {
                        command.CommandText = sqls[i];
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            WriteLog("Warning", string.Format("Помилка {0} оновлення бази даних {1}", ex.Message, sqls[i]));
                        }
                    }
                    _connectionWorking.Close();
                }
            }
        }
        #endregion

        #region Loging
        /// <summary>
        /// Write log to some table
        /// </summary>
        /// <param name="table">It can be the next: Debug, Error, Info, Trace, Warning</param>
        /// <param name="text"></param>
        public async void WriteLog(string table, string text)
        {
            await Task.Run(() =>
            {
                lock (_lockLoging)
                {
                    _connectionLoging.Open();
                    using (SqliteCommand command = _connectionLoging.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO " + table + " VALUES (" + (ulong)DateTime.Now.Subtract(new DateTime(2010, 1, 1)).TotalMilliseconds + ",'["
                        + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + DateTime.Now.TimeOfDay.Milliseconds + "] " + text.Replace('\'', '"') + "')";
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Log.Error("WritingLogError", ex.Message);
                        }
                        finally
                        {
                            _connectionLoging.Close();
                        }
                    }
                }
            });
        }

        public async void DeleteLogs(string table, DateTime toDate)
        {
            await Task.Run(() =>
            {
                lock (_lockLoging)
                {
                    _connectionLoging.Open();
                    using (SqliteCommand command = _connectionLoging.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM " + table + " WHERE Date < " + (ulong)toDate.Subtract(new DateTime(2010, 1, 1)).TotalMilliseconds;
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Log.Error("WritingLogError", ex.ToString());
                        }
                        finally
                        {
                            _connectionLoging.Close();
                        }
                    }
                }
            });
        }

        public string[] GetLogs(string table1)
        {
            lock (_lockLoging)
            {
                var table = new DataTable();
                using (SqliteCommand command = _connectionLoging.CreateCommand())
                {
                    command.CommandText = "SELECT Text FROM " + table1 + " ORDER BY Date";
                    table.TableName = "1";
                    var p = new SqliteDataAdapter(command);
                    try
                    {
                        p.Fill(table);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("WritingLogError", ex.ToString());
                        return new string[1] { "Error getting logs" };
                    }
                    finally
                    {
                        _connectionLoging.Close();
                    }
                }
                List<string> list = new List<string>();
                foreach (DataRow row in table.Rows)
                {
                    list.Add(row[0].ToString());
                }
                return list.ToArray();
            }
        }
        #endregion
    }
}