﻿using Android.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Controls
{
    public class GroupBox:StackLayout
    {
        private object _lockingObject;
        public GroupBox(/*StackOrientation orientation*/)
        {
            // Orientation = orientation;
            Orientation = StackOrientation.Vertical;
            _lockingObject = new object();
        }

        protected override void OnChildAdded(Element child)
        {
            if(child is RadioButton)
            {
                RadioButton radioButton = child as RadioButton;
                radioButton.SetLockingObject(_lockingObject);
                radioButton.OnChangeChecked += RadioButton_OnChangeChecked;
            }
                
            base.OnChildAdded(child);
        }

        private void RadioButton_OnChangeChecked(RadioButton radioButton, bool isChecked)
        {
            if(isChecked)
            {
                foreach(View view in Children)
                {
                    if (view != radioButton && view is RadioButton) (view as RadioButton).UnCheck();
                }
            }
        }
    }
}
