﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;

namespace Controls
{
    public class Preloader : AbsoluteLayout
    {
        SKCanvasView _canvas;
        AbsoluteLayout _absoluteLayout;
        Label _label;
        float _startAngle;

        public Preloader()
        {
            Padding = 20;
            IsVisible = false;
            Frame frame = new Frame();
            frame.BackgroundColor = Color.Ivory;
            frame.CornerRadius = 10;
            frame.Padding = 5;
            Grid grid = new Grid();
            grid.Margin = 5;
            ColumnDefinitionCollection columnDefinitions = new ColumnDefinitionCollection();
            ColumnDefinition columnDefinition1 = new ColumnDefinition();
            columnDefinition1.Width = new GridLength(8, GridUnitType.Star);
            ColumnDefinition columnDefinition2 = new ColumnDefinition();
            columnDefinition2.Width = new GridLength(1, GridUnitType.Star);
            columnDefinitions.Add(columnDefinition1);
            columnDefinitions.Add(columnDefinition2);
            grid.ColumnDefinitions = columnDefinitions;
            _absoluteLayout = new AbsoluteLayout();
            _canvas = new SKCanvasView();
            _canvas.PaintSurface += Canvas_PaintSurface;
            grid.Children.Add(_absoluteLayout, 0, 0);
            grid.Children.Add(_canvas, 1, 0);
            double minSize = Width < Height ? Width : Height;
            AbsoluteLayout.SetLayoutBounds(frame, new Rectangle(0.5, 0.5, minSize, minSize / 10));
            AbsoluteLayout.SetLayoutFlags(frame, AbsoluteLayoutFlags.PositionProportional);
            frame.Content = grid;
            Children.Add(frame);
            BackgroundColor = Color.FromRgba(0, 0, 0, 125);
            _label = new Label();
            AbsoluteLayout.SetLayoutBounds(this, new Rectangle(0, 0, 1, 1));
            AbsoluteLayout.SetLayoutFlags(this, AbsoluteLayoutFlags.All);
        }

        public new bool IsVisible
        {
            get { return base.IsVisible; }
            private set { base.IsVisible = value; }
        }

        public void Show()
        {
            //if (IsVisible) return;
            IsVisible = true;
            _startAngle = 0;
            Device.StartTimer(TimeSpan.FromMilliseconds(30), Timer_Elapsed);
        }

        public void Close()
        {
            IsVisible = false;
        }

        private void Canvas_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Ivory);
            SKRect rect;
            canvas.GetLocalClipBounds(out rect);
            float dx, dy;
            dx = rect.Width / 11;
            dy = rect.Height / 11;
            rect.Left = rect.Left + dx;
            rect.Top = rect.Top + dy;
            rect.Right = rect.Right - dx;
            rect.Bottom = rect.Bottom - dy;
            SKPaint paint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = Color.Fuchsia.ToSKColor(),
                StrokeWidth = rect.Width / 6,
                StrokeCap = SKStrokeCap.Round
            };
            SKPath path = new SKPath();
            path.AddArc(rect, _startAngle, 120);
            canvas.DrawPath(path, paint);
        }

        private bool Timer_Elapsed()
        {
            _canvas.InvalidateSurface();
            _startAngle += 10;
            return IsVisible;
        }

        public string Text
        {
            get { return _label.Text; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _label.Text = value;
                    _label.TextColor = Color.DarkGray;
                    _label.FontSize = 16;
                }
                _absoluteLayout.Children.Add(_label, new Point(0.5, 0.5));
                AbsoluteLayout.SetLayoutFlags(_label, AbsoluteLayoutFlags.PositionProportional);
            }
        }
    }
}
