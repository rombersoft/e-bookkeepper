﻿using Android.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Controls
{
    public class RadioButton : Grid
    {
        private Image _image;
        private ImageSource _checkedImg, _uncheckedImg;
        private bool _checked;
        private Label _label;
        private object _lockingObject;
        public event Action<RadioButton, bool> OnChangeChecked;

        public RadioButton() : base()
        {
            this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(9, GridUnitType.Star) });
            this.Padding = new Thickness(0, 10);
            this.BackgroundColor = Color.Transparent;
            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped;
            this.GestureRecognizers.Add(tapGestureRecognizer);
            _checkedImg = ImageSource.FromResource("Controls.Images.RadioButton.checked.png");
            _uncheckedImg = ImageSource.FromResource("Controls.Images.RadioButton.unchecked.png");
            _image = new Image();
            _image.Source = _uncheckedImg;
            this.Children.Add(_image, 0, 0);
            this.PropertyChanged += RadioButton_PropertyChanged;
        }

        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                if (_checked) _image.Source = _checkedImg;
                else _image.Source = _uncheckedImg;
                if (OnChangeChecked != null) OnChangeChecked(this, value);
            }
        }

        public void Check()
        {
            if (Checked != true) Checked = true;
        }

        internal void UnCheck()
        {
            if (Checked != false) Checked = false;
        }

        internal void SetLockingObject(object obj)
        {
            _lockingObject = obj;
        }

        private void RadioButton_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "BackgroundColor")
            {

            }
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            lock(_lockingObject)
            {
                Check();
            }
        }

        public string Text
        {
            get
            {
                return _label == null ? "" : _label.Text;
            }
            set
            {
                if (_label == null)
                {
                    _label = new Label();
                    _label.FontSize = 20;
                    _label.VerticalOptions = LayoutOptions.Center;
                    _label.HorizontalOptions = LayoutOptions.Start;
                    this.Children.Add(_label, 1, 0);
                }
                _label.Text = value;
            }
        }
    }
}
