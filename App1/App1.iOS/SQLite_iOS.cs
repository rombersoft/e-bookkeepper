﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Foundation;
using UIKit;
using App1.DataStorage;
using App1.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_iOS))]
namespace App1.iOS
{
    public class SQLite_iOS : ISqlite
    {
        public SQLite_iOS()
        {

        }

        public string GetDatabasePath(string sqliteFilename)
        {
            // определяем путь к бд
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryPath = Path.Combine(documentsPath, "..", "Library"); // папка библиотеки
            var path = Path.Combine(libraryPath, sqliteFilename);
            return path;
        }
    }
}