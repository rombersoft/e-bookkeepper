﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{

    class ArcForStatistic
    {
        public string Name { get; set; }
        public float Angle { get; set; }
        public SKColor Color { get; set; }

        public ArcForStatistic(string name, float angle, SKColor color)
        {
            Name = name;
            Angle = angle;
            Color = color;
        }
    }
}
