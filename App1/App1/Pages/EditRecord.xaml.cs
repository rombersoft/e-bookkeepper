﻿using App1.DataStorage;
using App1.Resx;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditRecord : ContentPage
    {
        private Record _record;
        private bool _isNoteChanged, _isCostChanged, _isDeleted;
        public event Action RecordChanged;
        public event Action RecordDeleted;

        public EditRecord()
        {
            InitializeComponent();
            this.Disappearing += Page_Disappearing;
        }

        private async void Page_Disappearing(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                if (_isCostChanged || _isNoteChanged)
                {
                    RecordDataStorage storage = new RecordDataStorage();
                    if (_isCostChanged) _record.Price = Single.Parse(_entryCost.Text, CultureInfo.InvariantCulture);
                    if (_isNoteChanged) _record.Note = _editorNote.Text;
                    if (storage.Store(_record)) DisplayAlert(string.Empty, Resource.message19, "Ok");
                    else DisplayAlert(string.Empty, Resource.message20, "Ok");
                    if (RecordChanged != null) RecordChanged();
                }
                else if(_isDeleted && RecordDeleted != null) RecordDeleted();
            });
        }

        internal void FillRecord(Record record)
        {
            _record = record;
            _editorNote.Text = record.Note;
            _entryCost.Text = record.Price.ToString(CultureInfo.InvariantCulture);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            RecordDataStorage storage = new RecordDataStorage();
            if(sender == _butDelete)
            {
                if(storage.Delete(_record.Id))
                {
                    await DisplayAlert(string.Empty, Resource.message21, "Ok");
                    _isCostChanged = _isNoteChanged = false;
                    _isDeleted = true;
                    await this.Navigation.PopAsync();
                }
                else await DisplayAlert(string.Empty, Resource.message22, "Ok");
            }
        }

        private void Cost_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(_entryCost.Text))
            {
                if (e.NewTextValue[e.NewTextValue.Length - 1] == ',')
                    _entryCost.Text = _entryCost.Text.Replace(',', '.');
                if (_entryCost.Text.IndexOf('.') != _entryCost.Text.LastIndexOf('.'))
                {
                    _entryCost.Text = _entryCost.Text.Remove(_entryCost.Text.Length - 1);
                }
                if (e.NewTextValue == ".") _entryCost.Text = "0." + e.NewTextValue;
                if (e.NewTextValue == "-") _entryCost.Text = string.Empty;
                if (_record.Price.ToString(CultureInfo.InvariantCulture) != _entryCost.Text) _isCostChanged = true;
            }
        }

        private void Note_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_editorNote.Text != _record.Note) _isNoteChanged = true;
            else _isNoteChanged = false;
        }
    }
}