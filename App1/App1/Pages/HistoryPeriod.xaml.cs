﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Controls;
using App1.Resx;
using App1.DataStorage;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPeriod : ContentPage
    {
        private Frame _frame;
        private StackLayout _stackDate;
        private RadioButton _radioButtonDay, _radioButtonMonth, _radioButtonPeriod, _radioButtonLastWeek;
        private DatePicker _datePickerFrom, _datePickerTo;
        public HistoryPeriod()
        {
            InitializeComponent();

            GroupBox groupBox = new GroupBox();
            _radioButtonDay = new RadioButton();
            _radioButtonDay.Text = Resource.forToday;

            _radioButtonLastWeek = new RadioButton();
            _radioButtonLastWeek.Text = Resource.lastWeek;
           
            _radioButtonMonth = new RadioButton();
            _radioButtonMonth.Text = Resource.forCurrentMonth;
            
            _radioButtonPeriod = new RadioButton();
            _radioButtonPeriod.Text = Resource.forSelectedPeriod;
            _radioButtonPeriod.OnChangeChecked += RadioButtonPeriod_OnChangeChecked;

            groupBox.Children.Add(_radioButtonDay);
            groupBox.Children.Add(_radioButtonLastWeek);
            groupBox.Children.Add(_radioButtonMonth);
            groupBox.Children.Add(_radioButtonPeriod);

            _frame = new Frame();
            _frame.BackgroundColor = Color.AliceBlue;
            _frame.IsVisible = false;

            _stackDate = new StackLayout();
            Label labelFromDate = new Label();
            labelFromDate.Text = Resource.fromDate;
            _datePickerFrom = new DatePicker();
            _datePickerFrom.Format = "D";
            _datePickerFrom.MinimumDate = new DateTime(2010, 1, 1);
            _datePickerFrom.MaximumDate = new DateTime(2083, 4, 27);
            _datePickerFrom.DateSelected += DatePicker_DateSelected;

            Label labelToDate = new Label();
            labelToDate.Text = Resource.toDate;
            _datePickerTo = new DatePicker();
            _datePickerTo.Format = "D";
            _datePickerTo.MinimumDate = new DateTime(2010, 1, 1);
            _datePickerTo.MaximumDate = new DateTime(2083, 4, 27);
            _datePickerTo.DateSelected += DatePicker_DateSelected;

            _stackDate.Children.Add(labelFromDate);
            _stackDate.Children.Add(_datePickerFrom);
            _stackDate.Children.Add(labelToDate);
            _stackDate.Children.Add(_datePickerTo);

            _frame.Content = _stackDate;

            Button butShow = new Button();
            butShow.Text = Resource.show;
            butShow.CornerRadius = 30;
            butShow.FontSize = 20;
            butShow.HeightRequest = 60;
            butShow.BackgroundColor = Color.RosyBrown;
            butShow.Clicked += ButShow_Clicked;
            
            _stackLayout.Children.Add(groupBox);
            _stackLayout.Children.Add(_frame);
            _stackLayout.Children.Add(butShow);
        }

        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (sender == _datePickerFrom) _datePickerTo.MinimumDate = _datePickerFrom.Date.AddDays(1);
            else _datePickerFrom.MaximumDate = _datePickerTo.Date.AddDays(-1);
        }

        private async void ButShow_Clicked(object sender, EventArgs e)
        {
            Record[] records = null;
            RecordDataStorage storage = new RecordDataStorage();
            if (_radioButtonDay.Checked) records = storage.GetForToday();
            else if (_radioButtonLastWeek.Checked) records = storage.GetForPeriod(DateTime.Now.AddDays(-7), DateTime.Now);
            else if (_radioButtonMonth.Checked) records = storage.GetForCurrentMonth();
            else if (_radioButtonPeriod.Checked) records = storage.GetForPeriod(_datePickerFrom.Date, _datePickerTo.Date);
            if (records.Length == 0)
                await DisplayAlert("", Resource.noData, "Ok");
            else await Navigation.PushAsync(new HistoryList(records), true);
        }

        private void RadioButtonPeriod_OnChangeChecked(RadioButton radioButton, bool isChecked)
        {
            if (isChecked) _frame.IsVisible = true;
            else _frame.IsVisible = false;
        }
    }
}