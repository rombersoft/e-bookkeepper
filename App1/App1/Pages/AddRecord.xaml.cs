﻿using App1.DataStorage;
using App1.Resx;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddRecord : ContentPage
    {
        private Category[] _categories;
        private GoodsService[] _gs;
        private RecordDataStorage _recordDataStorage;
        private Action _action;
        private List<ushort> _indexes;
        internal event Action<float> OnRecordAdded;

        public AddRecord()
        {
            InitializeComponent();
            _recordDataStorage = new RecordDataStorage();
            _indexes = new List<ushort>();
            UpdateCategories();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if(_pickerGS.SelectedIndex == -1)
            {
                DisplayAlert(string.Empty, Resource.message8, "Ok");
                return;
            }
            if(string.IsNullOrEmpty(_entryCost.Text))
            {
                DisplayAlert(string.Empty, Resource.message9, "Ok");
                return;
            }
            _butAdd.IsEnabled = false;
            float cost = Single.Parse(_entryCost.Text, CultureInfo.InvariantCulture);
            if (_recordDataStorage.Store(new Record() {Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm"), IdGoodsServices = _pickerGS.SelectedItem as string, Note = _editorComment.Text, Price = cost}))
            {
                DisplayAlert(string.Empty, Resource.message10, "Ok");
                _entryCost.Text = string.Empty;
                _editorComment.Text = string.Empty;
                if (OnRecordAdded != null) OnRecordAdded(cost);
            }
            else DisplayAlert(string.Empty, Resource.message27, "Ok");
            _butAdd.IsEnabled = true;
        }

        private async void SelectedIndexChanged(object sender, EventArgs e)
        {
            if(((sender as Picker).SelectedItem as string) == "++++++")
            {
                if (sender == _pickerCat)
                {
                    try
                    {
                        await Navigation.PushAsync(new CategoryManagement(), true);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                    _action = UpdateCategories;
                }
                else
                {
                    try
                    {
                        await Navigation.PushAsync(new NameManagement(), true);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                    _action = UpdateNominations;
                }
            }
            else
            {
                if (sender == _pickerCat && (sender as Picker).SelectedIndex >=0)
                {
                    UpdateNominations();
                }
            }
        }

        private void UpdateCategories()
        {
            _pickerCat.Items.Clear();
            _indexes.Clear();
            _categories = new CategoryDataStorage().GetAll();
            for (ushort i = 0; i < _categories.Length; i++)
            {
                _pickerCat.Items.Add(_categories[i].Name);
                _indexes.Add(_categories[i].Id);
            }
            _pickerCat.Items.Add("++++++");
        }

        private void UpdateNominations()
        {
            _pickerGS.Items.Clear();
            _gs = new NominationDataStorage().GetByCategory(_indexes[_pickerCat.SelectedIndex]);
            for (ushort i = 0; i < _gs.Length; i++)
            {
                _pickerGS.Items.Add(_gs[i].GsName);
            }
            _pickerGS.Items.Add("++++++");
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            try
            {
                if (_action != null) _action.Invoke();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        private void Cost_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(!string.IsNullOrEmpty(_entryCost.Text))
            {
                if (e.NewTextValue[e.NewTextValue.Length - 1] == ',')
                    _entryCost.Text = _entryCost.Text.Replace(',', '.');
                if (_entryCost.Text.IndexOf('.') != _entryCost.Text.LastIndexOf('.'))
                {
                    _entryCost.Text = _entryCost.Text.Remove(_entryCost.Text.Length - 1);
                }
                if (e.NewTextValue == ".") _entryCost.Text = "0." + e.NewTextValue;
                if (e.NewTextValue == "-") _entryCost.Text = string.Empty;
            }
        }

        /*private async void EntryCost_Focused(object sender, FocusEventArgs e)
        {
            await _scrollView.ScrollToAsync(0, 100, true);
        }*/
    }
}