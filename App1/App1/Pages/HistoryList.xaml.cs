﻿using App1.DataStorage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryList : ContentPage
    {
        private List<Record> _records, _sortedRecords;
        private Record _recordForEditing;
        private float _sum;

        public HistoryList()
        {
            InitializeComponent();
        }

        public HistoryList(Record[] records)
        {
            _records = records.ToList();
            InitializeComponent();
            Fill(_records);
        }

        private void Fill(IEnumerable<Record> records)
        {
            _sortedRecords = new List<Record>(records);
            RowDefinition rowDefinition = new RowDefinition();
            rowDefinition.Height = new GridLength(40, GridUnitType.Absolute);
            float total = 0;
            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped;
            for (int i = 0; i < records.Count(); i++)
            {
                _grid.RowDefinitions.Add(rowDefinition);
                Label label = new Label();
                label.BindingContext = records.ElementAt(i);
                label.FontSize = 10;
                label.Margin = new Thickness(5);
                label.HorizontalOptions = LayoutOptions.StartAndExpand;
                label.VerticalOptions = LayoutOptions.Center;
                label.Text = records.ElementAt(i).IdGoodsServices;
                _grid.Children.Add(label, 0, i);
                label = new Label();
                label.FontSize = 10;
                label.Margin = new Thickness(5);
                label.HorizontalOptions = LayoutOptions.StartAndExpand;
                label.VerticalOptions = LayoutOptions.Center;
                label.Text = records.ElementAt(i).CategoryName;
                _grid.Children.Add(label, 1, i);
                label = new Label();
                label.BackgroundColor = Color.Ivory;
                label.FontSize = 16;
                label.Margin = new Thickness(5);
                label.HorizontalOptions = LayoutOptions.CenterAndExpand;
                label.VerticalOptions = LayoutOptions.Center;
                string price = records.ElementAt(i).Price.ToString(CultureInfo.InvariantCulture);
                switch(price.Length)
                {
                    case 1:
                        label.Text = "    " + price + "    ";
                        break;
                    case 2:
                        label.Text = "   " + price + "   ";
                        break;
                    case 3:
                        label.Text = "  " + price + "  ";
                        break;
                    case 4:
                        label.Text = " " + price + " ";
                        break;
                    default:
                        label.Text = price;
                        break;
                }
                label.GestureRecognizers.Add(tapGestureRecognizer);
                total += records.ElementAt(i).Price;
                _grid.Children.Add(label, 2, i);
                label = new Label();
                label.FontSize = 10;
                label.Margin = new Thickness(5);
                label.HorizontalOptions = LayoutOptions.EndAndExpand;
                label.VerticalOptions = LayoutOptions.Center;
                label.Text = records.ElementAt(i).Date;
                _grid.Children.Add(label, 3, i);
                Button button = new Button();
                button.Image = "options.png";
                button.Margin = new Thickness(0);
                button.HorizontalOptions = LayoutOptions.End;
                button.BackgroundColor = Color.Transparent;
                button.BorderColor = Color.Transparent;
                button.BorderWidth = 0;
                button.CornerRadius = 30;
                int n = i;
                button.Clicked += async (sender, e)=> 
                {
                    EditRecord editRecord = new EditRecord();
                    _recordForEditing = records.ElementAt(n);
                    editRecord.FillRecord(_recordForEditing);
                    editRecord.RecordChanged += RecordWasChanged;
                    editRecord.RecordDeleted += ()=>
                    {
                        RecordWasDeleted();
                    };
                    await Navigation.PushAsync(editRecord, true);
                };
                _grid.Children.Add(button, 4, i);
            }
            _labelTotal.Text = Resx.Resource.total + ": " +  total.ToString(CultureInfo.InvariantCulture);
        }

        private async void RecordWasChanged()
        {
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(UpdateCrossThreading);                
            });
        }

        private async void RecordWasDeleted()
        {
            await Task.Run(() =>
            {
                _sortedRecords.Remove(_recordForEditing);
                _records.Remove(_recordForEditing);
                Device.BeginInvokeOnMainThread(UpdateCrossThreading);
            });
        }

        private void UpdateCrossThreading()
        {
            _grid.Children.Clear();
            _grid.RowDefinitions.Clear();
            _labelTotal.Text = string.Empty;
            Fill(_sortedRecords);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (_records.Count < 3) return;
            IEnumerable<Record> records = null;
            if(sender == _butNomination)
            {
                records = _records.OrderBy(x => x.IdGoodsServices);
            }
            else if(sender == _butCategory)
            {
                records = _records.OrderBy(x => x.CategoryName);
            }
            else if(sender == _butPrice)
            {
                records = _records.OrderBy(x => x.Price);
            }
            else if(sender == _butDate)
            {
                records = _records;
            }
            _grid.Children.Clear();
            _grid.RowDefinitions.Clear();
            _labelTotal.Text = string.Empty;
            if(records != null) Fill(records);
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Label label = sender as Label;
            if (label.BackgroundColor == Color.Ivory)
            {
                label.BackgroundColor = Color.WhiteSmoke;
                _sum += Single.Parse(label.Text.Trim(), CultureInfo.InvariantCulture);
            }
            else
            {
                label.BackgroundColor = Color.Ivory;
                _sum -= Single.Parse(label.Text.Trim(), CultureInfo.InvariantCulture);
            }
            if (_sum > 0) _labelSum.Text = Resx.Resource.selectedSum + ": " + _sum.ToString(CultureInfo.InvariantCulture);
            else _labelSum.Text = string.Empty;
        }
    }
}