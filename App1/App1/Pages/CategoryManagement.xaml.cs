﻿using App1.DataStorage;
using App1.Resx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CategoryManagement : ContentPage
	{
        private Category[] _categories;
        private CategoryDataStorage _categoryDataStorage;
        private List<ushort> _indexes;

        public CategoryManagement ()
		{
			InitializeComponent ();
            _indexes = new List<ushort>(50);
            _categoryDataStorage = new CategoryDataStorage();
            UpdateList();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if(sender == _butAdd)
            {
                if (string.IsNullOrEmpty(_entryNewCategory.Text)) await DisplayAlert("", Resource.message4, "Ok");
                else
                {
                    string temp = _entryNewCategory.Text.ToLower().Trim();
                    if (_pickerCat.Items.Where(x=>x.ToLower() == temp).Count() > 0)
                        await DisplayAlert("", Resource.message5 + ". " + Resource.message4, "Ok");
                    else if(_categoryDataStorage.Store(new Category() { Name = _entryNewCategory.Text.Trim()}))
                    {
                        await DisplayAlert(string.Empty, Resource.message11, "Ok");
                        _entryNewCategory.Text = string.Empty;
                        UpdateList();
                    }
                    else await DisplayAlert(string.Empty, String.Format(Resource.message14, _pickerCat.SelectedItem), "Ok");
                }
                    
            }
            else if(sender == _butDel)
            {
                if (_pickerCat.SelectedIndex == -1) await DisplayAlert("", Resource.message3, "Ok");
                else
                {
                    bool deleted = _categoryDataStorage.Delete(_indexes[_pickerCat.SelectedIndex]);
                    if (deleted)
                    {
                        await DisplayAlert(string.Empty, String.Format(Resource.message12, _pickerCat.SelectedItem), "Ok");
                        UpdateList();
                    }
                    else await DisplayAlert(string.Empty, String.Format(Resource.message13, _pickerCat.SelectedItem), "Ok");
                }
            }
        }

        private void UpdateList()
        {
            _indexes.Clear();
            _pickerCat.Items.Clear();
            _categories = _categoryDataStorage.GetAll();
            for (ushort i = 0; i < _categories.Length; i++)
            {
                _pickerCat.Items.Add(_categories[i].Name);
                _indexes.Add(_categories[i].Id);
            }
        }
    }
}