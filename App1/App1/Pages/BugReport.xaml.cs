﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using App1.Resx;
using App1.DataStorage;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BugReport : ContentPage
    {
        private byte[] _buff;
        private static BugReport _instance;

        public BugReport()
        {
            InitializeComponent();
            _editorSteps.Text = "1." + Environment.NewLine + "2." + Environment.NewLine + "3.";
            _instance = this;
            if(Commons.Instance.Options.WriteLog) Appearing += BugReport_Appearing;
        }

        private void BugReport_Appearing(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            Task.Run(() =>
            {
                string[] logs = Logs.GetAll(Logs.TraceType);
                if(logs.Length > 0)
                {
                    DependencyService.Get<IWcfService>().UploadLogs("E-Bookkeepper", "Ident", "Application 1.1.1; Android " + Android.OS.Build.VERSION.Release + ";" +
                        " Brand " + Android.OS.Build.Brand + "; Model " + Android.OS.Build.Model, logs);
                    Logs.Delete(Logs.TraceType, now);
                }
            });
        }

        public static void SetFileData(byte[] buff, string fileName)
        {
            _instance._fileName.Text = fileName;
            _instance._buff = buff;
        }

        private async void OnButtonClicked(object sender, EventArgs e)
        {
            if(sender == _butLoadScreenShot)
            {
                await DependencyService.Get<IMediaService>().OpenGallery();
            }
            else if(sender == _butSendReport)
            {
                if(string.IsNullOrEmpty(_editorActualBehaviour.Text) || _editorActualBehaviour.Text.Length < 20)
                {
                    await DisplayAlert(string.Empty, Resource.actualBehaviour + " " + Resource.mustHave20SymbolsAsLess, "Ok");
                    return;
                }
                if(string.IsNullOrEmpty(_editorDescription.Text) || _editorDescription.Text.Length < 20)
                {
                    await DisplayAlert(string.Empty, Resource.description + " " + Resource.mustHave20SymbolsAsLess, "Ok");
                    return;
                }
                if (string.IsNullOrEmpty(_editorExpectedBehaviour.Text) || _editorExpectedBehaviour.Text.Length < 20)
                {
                    await DisplayAlert(string.Empty, Resource.expectedBehaviour + " " + Resource.mustHave20SymbolsAsLess, "Ok");
                    return;
                }
                if (string.IsNullOrEmpty(_editorSteps.Text) || _editorSteps.Text.Length < 20)
                {
                    await DisplayAlert(string.Empty, Resource.stepsToReproduce + " " + Resource.mustHave20SymbolsAsLess, "Ok");
                    return;
                }
                uint result=2;
                _preloader.Show();
                try
                {
                    result = await DependencyService.Get<IWcfService>().SendReport("Ident", "E-Bookkeepper", _entryEmail.Text, _editorDescription.Text + Environment.NewLine + "### STEPS ###" + Environment.NewLine + _editorSteps.Text +
                    Environment.NewLine + "### ACTUAL BEHAVIOUR ###" + Environment.NewLine + _editorActualBehaviour.Text +
                    Environment.NewLine + "### EXPECTED BEHAVIOUR ###" + Environment.NewLine + _editorExpectedBehaviour, _buff);
                }
                catch(Exception)
                {
                    result = 2;
                }
                finally
                {
                    _preloader.Close();
                }
                switch(result)
                {
                    case 0:
                        await DisplayAlert(string.Empty, Resource.message24, "Ok");
                        break;
                    case 1:
                        await DisplayAlert(string.Empty, Resource.message25, "Ok");
                        break;
                    case 2:
                        await DisplayAlert(string.Empty, Resource.message26, "Ok");
                        break;
                }
            }
        }
    }
}