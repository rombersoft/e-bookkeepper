﻿using App1.DataStorage;
using App1.Resx;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{
        private SettingsDataStorage _storage;
        private Action<bool> _setShowResidue;

        public SettingsPage (Action<bool> setShowResidue)
		{
			InitializeComponent ();
            _setShowResidue = setShowResidue;
            _storage = new SettingsDataStorage();
            
            _switchOnBudget.On = Commons.Instance.Options.UseBudget;
            _switchShowBudget.On = Commons.Instance.Options.ShowResidue;
            _switchOnSecurity.On = Commons.Instance.Options.UseSecurity;
            _switchOnLoging.On = Commons.Instance.Options.WriteLog;
            _entryAnswer.Text = Commons.Instance.Options.Answer;
            _entryBudgetAmount.Text = Commons.Instance.Options.Budget.ToString(CultureInfo.InvariantCulture);
            _entryCtrlWord.Text = Commons.Instance.Options.CtrlWord;
            _entryPassword.Text = Commons.Instance.Options.Password;
            this.Disappearing += SettingsPage_Disappearing;
		}

        private void SettingsPage_Disappearing(object sender, EventArgs e)
        {
            if (_switchOnBudget.On)
            {
                if (!string.IsNullOrEmpty(_entryBudgetAmount.Text))
                {
                    float budget = Single.Parse(_entryBudgetAmount.Text, CultureInfo.InvariantCulture);
                    if (budget > 1f)
                    {
                        Commons.Instance.Options.UseBudget = true;
                        Commons.Instance.Options.Budget = budget;
                    }
                    else Commons.Instance.Options.UseBudget = false;
                    Commons.Instance.Options.ShowResidue = _switchShowBudget.On;
                }
                else Commons.Instance.Options.UseBudget = false;
            }
            else Commons.Instance.Options.UseBudget = false;
            if (_switchOnSecurity.On)
            {
                if (!string.IsNullOrEmpty(_entryPassword.Text))
                {
                    Commons.Instance.Options.UseSecurity = true;
                    Commons.Instance.Options.Password = _entryPassword.Text;
                }
                else Commons.Instance.Options.UseSecurity = false;
                if (!string.IsNullOrEmpty(_entryCtrlWord.Text) && !string.IsNullOrEmpty(_entryAnswer.Text))
                {
                    Commons.Instance.Options.Answer = _entryAnswer.Text;
                    Commons.Instance.Options.CtrlWord = _entryCtrlWord.Text;
                }
                else
                {
                    Commons.Instance.Options.Answer = null;
                    Commons.Instance.Options.CtrlWord = null;
                }
            }
            else Commons.Instance.Options.UseSecurity = false;
            Commons.Instance.Options.WriteLog = _switchOnLoging.On;
            Commons.Instance.StoreOptions();
            _setShowResidue(Commons.Instance.Options.UseBudget && Commons.Instance.Options.ShowResidue);
        }       

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if(sender == _butCat) await Navigation.PushAsync(new CategoryManagement(), true);
            else if(sender == _butName)
            {
                if (new CategoryDataStorage().GetCount() > 0) await Navigation.PushAsync(new NameManagement(), true);
                else await DisplayAlert(Resource.denied, Resource.message1, "Ok");
            }
            else if(sender == _butClearDB)
            {
                bool result = await DisplayAlert(Resource.clearDb, Resource.message2, Resource.yes, Resource.no);
                if (result)
                    ProxiDb.Instance.ClearDb();
            }
            else if(sender == _butCopyDB)
            {
                await DependencyService.Get<IMediaService>().CreateDbCopy();
            }
            else if(sender == _butRestoreDB)
            {
                bool result = await DisplayAlert(Resource.restoringDb, Resource.message28, Resource.yes, Resource.no);
                if(result)
                {
                    //await DependencyService.Get<IMediaService>().OpenDB();
                    FileData fileData = await CrossFilePicker.Current.PickFile();
                    if(!fileData.FileName.EndsWith(".db3"))
                        await DisplayAlert(Resource.errorTryAgain, Resource.message30, "Ok");
                    DependencyService.Get<IMediaService>().RestoreDB(fileData.DataArray);
                }                    
            }
        }

        private void SwitchCell_OnChanged(object sender, ToggledEventArgs e)
        {
            if(sender == _switchOnBudget)
            {
                if(e.Value)
                {
                    _switchShowBudget.IsEnabled = true;
                    _entryBudgetAmount.IsEnabled = true;
                }
                else
                {
                    _switchShowBudget.IsEnabled = false;
                    _entryBudgetAmount.IsEnabled = false;
                }
            }
            else if(sender == _switchOnSecurity)
            {
                if (e.Value)
                {
                    _entryPassword.IsEnabled = true;
                    _entryCtrlWord.IsEnabled = true;
                    _entryAnswer.IsEnabled = true;
                }
                else
                {
                    _entryPassword.IsEnabled = false;
                    _entryCtrlWord.IsEnabled = false;
                    _entryAnswer.IsEnabled = false;
                }
            }
        }
    }
}