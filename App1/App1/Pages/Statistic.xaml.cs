﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using App1.DataStorage;
using App1.Resx;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Statistic : ContentPage
    {
        private List<ushort> _indexes;
        private Category[] _categories;
        ArcForStatistic[] _arcs;
        float _totalMoney;

        public Statistic()
        {
            InitializeComponent();
            _byCategories.Check();
            _indexes = new List<ushort>(200);
            UpdateCategories();
            SizeChanged += Statistic_SizeChanged;
        }

        private void Statistic_SizeChanged(object sender, EventArgs e)
        {
            if(_arcs != null && _arcs.Length > 0) Update();
        }

        private void SKView_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            canvas.Clear(SKColors.Ivory);
            SKRect rect;
            canvas.GetLocalClipBounds(out rect);
            float dx, dy;
            dx = rect.Width /6;
            dy = rect.Height /6;
            rect.Left = rect.Left + dx;
            rect.Top = rect.Top + dy;
            rect.Right = rect.Right - dx;
            rect.Bottom = rect.Bottom - dy;
            float lastPosition = 180;
            if(_arcs != null)
            {
                for (byte i=0; i<_arcs.Length; i++)
                {
                    SKPaint paint = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        Color = _arcs[i].Color,
                        StrokeWidth = rect.Width/2.5f
                    };
                    SKPath path = new SKPath();
                    path.AddArc(rect, lastPosition, _arcs[i].Angle);
                    canvas.DrawPath(path, paint);
                    lastPosition += _arcs[i].Angle;
                }
                canvas.DrawText(((int)_totalMoney).ToString(), rect.Width / 1.7f, rect.Height / 1.3f, new SKPaint
                {
                    Style = SKPaintStyle.Fill,
                    Color = new SKColor(0, 0, 255),
                    TextSize = 80,
                    IsAntialias = true
                });
            }            
        }

        private void RadioButton_OnChangeChecked(Controls.RadioButton arg1, bool arg2)
        {
            _frame.IsVisible = _byCategory.Checked;
        }

        private void DateSelected(object sender, DateChangedEventArgs e)
        {
            if (sender == _datePickerFrom) _datePickerTo.MinimumDate = _datePickerFrom.Date.AddDays(1);
            else _datePickerFrom.MaximumDate = _datePickerTo.Date.AddDays(-1);
        }

        private void UpdateCategories()
        {
            _pickerCategory.Items.Clear();
            _indexes.Clear();
            _categories = new CategoryDataStorage().GetAll();
            for (ushort i = 0; i < _categories.Length; i++)
            {
                _pickerCategory.Items.Add(_categories[i].Name);
                _indexes.Add(_categories[i].Id);
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            _butUpdate.IsEnabled = false;
            Update();
        }

        private async void Update()
        {
            _containerGraphics.HeightRequest = _containerGraphics.Width;
            Dictionary<string, float> statistics = null;
            if (_byCategories.Checked) statistics = new RecordDataStorage().GetStatisticsByCategories(_datePickerFrom.Date, _datePickerTo.Date);
            else if (_byCategory.Checked)
            {
                if (_pickerCategory.SelectedIndex < 0)
                {
                    await DisplayAlert(string.Empty, Resource.message3, "Ok");
                    _butUpdate.IsEnabled = true;
                    return;
                }
                else statistics = new RecordDataStorage().GetStatisticsByCategory(_indexes[_pickerCategory.SelectedIndex], _datePickerFrom.Date, _datePickerTo.Date);
            }
            else if (_byName.Checked) statistics = new RecordDataStorage().GetStatisticsByName(_datePickerFrom.Date, _datePickerTo.Date);
            else
            {
                _butUpdate.IsEnabled = true;
                return;
            }
            if (statistics != null && statistics.Count > 0)
            {
                _grid.IsVisible = true;
                if (_grid.Children.Count > 0) _grid.Children.Clear();
                _containerGraphics.IsVisible = true;
                _totalMoney = 0;
                foreach (var key in statistics.Keys)
                {
                    _totalMoney += statistics[key];
                }
                _arcs = new ArcForStatistic[statistics.Count];
                short i = 0;
                foreach (var key in statistics.Keys)
                {
                    _arcs[i++] = new ArcForStatistic(key, 360f * (statistics[key] / _totalMoney), SKColor.Empty);
                }
                Array.Sort(_arcs, new Comparison<ArcForStatistic>((i1, i2) => i2.Angle.CompareTo(i1.Angle)));
                Random rnd = new Random(255);
                for (short j = 0; j < _arcs.Length; j++)
                {
                    SKColor color = Color.FromUint((uint)rnd.Next(5000, 16500000)).ToSKColor().WithAlpha(255);
                    _arcs[j].Color = color;
                    BoxView box = new BoxView();
                    box.Color = _arcs[j].Color.ToFormsColor();
                    _grid.Children.Add(box, 0, j);
                    Label label = new Label();
                    label.Text = Math.Round(statistics[_arcs[j].Name] / _totalMoney * 100, 2).ToString() + "% (" + statistics[_arcs[j].Name] + ") - " + _arcs[j].Name;
                    label.TextColor = Color.DarkGray;
                    label.FontSize = 16;
                    label.Margin = new Thickness(5);
                    label.HorizontalOptions = LayoutOptions.StartAndExpand;
                    label.VerticalOptions = LayoutOptions.Center;
                    _grid.Children.Add(label, 1, j);
                }
                _skSurface.InvalidateSurface();
            }
            else
            {
                _containerGraphics.IsVisible = false;
                await DisplayAlert(string.Empty, Resource.message23, "Ok");
            }
            _butUpdate.IsEnabled = true;
        }
    }
}