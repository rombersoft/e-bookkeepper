﻿using App1.DataStorage;
using App1.Resx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NameManagement : ContentPage
	{
        private GoodsService[] _gs;
        private NominationDataStorage _nominationDataStorage;
        private List<ushort> _indexes;

        public NameManagement ()
		{
			InitializeComponent ();
            _nominationDataStorage = new NominationDataStorage();
            _indexes = new List<ushort>(50);
            Category[] _categories = new CategoryDataStorage().GetAll();
            for (ushort i = 0; i < _categories.Length; i++)
            {
                _pickerCat.Items.Add(_categories[i].Name);
                _indexes.Add(_categories[i].Id);
            }
            UpdateNomination();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if(sender == _butAdd)
            {
                if (_pickerCat.SelectedIndex == -1)
                {
                    await DisplayAlert("", Resource.message3, "Ok");
                    return;
                }
                if(string.IsNullOrEmpty(_entryNewCategory.Text))
                {
                    await DisplayAlert("", Resource.message6, "Ok");
                    return;
                }
                string temp = _entryNewCategory.Text.ToLower().Trim();
                if (_pickerName.Items.Where(x => x.ToLower() == temp).Count() > 0) await DisplayAlert("", Resource.message7 + ". " + Resource.message6, "Ok");
                else if (_nominationDataStorage.Store(new GoodsService() { GsName = _entryNewCategory.Text.Trim(), IdCategory = _indexes[_pickerCat.SelectedIndex] }))
                {
                    await DisplayAlert(string.Empty, String.Format(Resource.message15, _pickerName.SelectedItem), "Ok");
                    _entryNewCategory.Text = string.Empty;
                    UpdateNomination();
                }
                else await DisplayAlert(string.Empty, String.Format(Resource.message16, _pickerName.SelectedItem), "Ok");
            }
            else if(sender == _butDel)
            {
                if (_pickerName.SelectedIndex == -1) await DisplayAlert(string.Empty, Resource.message8, "Ok");
                else if (_nominationDataStorage.Delete(_pickerName.SelectedItem as string))
                {
                    await DisplayAlert(string.Empty, String.Format(Resource.message17, _pickerName.SelectedItem), "Ok");
                    UpdateNomination();
                }
                else await DisplayAlert(string.Empty, String.Format(Resource.message18, _pickerName.SelectedItem), "Ok");
            }
        }

        void UpdateNomination()
        {
            _gs = _nominationDataStorage.GetAll();
            _pickerName.Items.Clear();
            for(ushort i=0; i<_gs.Length; i++)
            {
                _pickerName.Items.Add(_gs[i].GsName);
            }
        }
    }
}