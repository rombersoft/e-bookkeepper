﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Authorization : ContentPage
	{
		public Authorization ()
		{
			InitializeComponent ();
            _entryCtrlWord.Placeholder = Commons.Instance.Options.CtrlWord;
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            if(sender == _butLogin)
            {
                if (Commons.Instance.CheckPassword(_entryLogin.Text)) Navigation.PopModalAsync();
            }
            else if(sender == _butForgot)
            {
                _entryCtrlWord.IsVisible = true;
                _butCheckCtrlWord.IsVisible = true;
            }
            else if(sender == _butCheckCtrlWord)
            {
                if (Commons.Instance.CheckKeyWord(_entryCtrlWord.Text)) Navigation.PopModalAsync();
            }
        }
    }
}