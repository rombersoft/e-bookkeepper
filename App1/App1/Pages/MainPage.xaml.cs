﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using App1.Resx;
using App1.DataStorage;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        private short _startMenuPosition;
        private bool _menuIsShown;
        private PanGestureRecognizer _panGesture;
        private double _x0, _x1, _y0, _y1;
        private float _budgetResidue;

        public MainPage()
        {
            InitializeComponent();
            _menuIsShown = false;
            _startMenuPosition = (short)_menuPanel.Bounds.X;
            _panGesture = new PanGestureRecognizer();
            _panGesture.PanUpdated += TapGesture_Tapped;
            _mainContainer.GestureRecognizers.Add(_panGesture);
            this.Appearing += MainPage_Appearing;
            if (Commons.Instance.Options.UseBudget && Commons.Instance.Options.ShowResidue && Commons.Instance.Options.Budget > 1f)
            {
                ShowResidue(true);
            }
            IAdInterstitial adInterstitial = DependencyService.Get<IAdInterstitial>();
            adInterstitial.ShowAd();
        }

        private async void ShowResidue(bool show)
        {
            try
            {
                if (show)
                {
                    if (_labelResidue.IsVisible) return;
                    _labelResidue.IsVisible = true;
                    await Task.Run(() =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            _budgetResidue = Commons.Instance.Options.Budget - new RecordDataStorage().GetAmountForCurrentMonth();
                            _labelResidue.Text = Resource.residue + " : " + _budgetResidue;
                        });
                       
                    });
                }
                else
                {
                    if (!_labelResidue.IsVisible) return;
                    _labelResidue.IsVisible = false;
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        private async void MainPage_Appearing(object sender, EventArgs e)
        {
            if (Commons.Instance.Options.UseSecurity && Commons.Instance.AuthorizedAgo > 10)
                await Navigation.PushModalAsync(new Authorization(), true);
        }

        private void TapGesture_Tapped(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    _x0 = 0;
                    _x1 = 0;
                    _y0 = 0;
                    _y1 = 0;
                    break;
                case GestureStatus.Running:
                    if (_x0 == 0 && _y0 == 0)
                    {
                        _x0 = e.TotalX;
                        _y0 = e.TotalY;
                    }
                    else
                    {
                        _x1 = e.TotalX;
                        _y1 = e.TotalY;
                    }
                    break;
                case GestureStatus.Completed:
                    if(Math.Abs(_x1 - _x0) > 60)
                    {
                        if (_x1 > 0) ShowMenu();
                        else if (_x1 < 0) HideMenu();
                    }
                    break;
            }
        }

        private async void MenuOnSelected(object sender, EventArgs e)
        {
            try
            {
                if (sender == _historyMenu)
                {
                    await Navigation.PushAsync(new HistoryPeriod(), true);
                }
                else if (sender == _statisticMenu)
                {
                    Statistic stat = new Statistic();
                    await Navigation.PushAsync(stat, true);
                }
                else if (sender == _settingsMenu)
                {
                    SettingsPage settingsPage = new SettingsPage(ShowResidue);
                    await Navigation.PushAsync(settingsPage, true);
                }
                else if (sender == _bugMenu)
                {
                    BugReport bugReport = new BugReport();
                    await Navigation.PushAsync(bugReport, true);
                }
                HideMenu();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        private async void Add_Clicked(object sender, EventArgs e)
        {
            AddRecord addRecordPage = new AddRecord();
            addRecordPage.OnRecordAdded += (cost) =>
            {
                _budgetResidue -= cost;
                _labelResidue.Text = Resource.residue + " : " + _budgetResidue;
            };
            try
            {
                await Navigation.PushAsync(addRecordPage, true);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (!_menuIsShown)
            {
                ShowMenu();
            }
            else
            {
                HideMenu();
            }
        }

        private async void ShowMenu()
        {
            _menuIsShown = true;
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(()=>
                {
                    _menuPanel.TranslateTo(_menuPanel.Bounds.X * -1, 0, 200);
                });
            });
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    _butAdd.FadeTo(0, 200);
                });
            });
            await Task.Run(() =>
            {
                Task.Delay(200).Wait();
                Device.BeginInvokeOnMainThread(() =>
                {
                    _butAdd.IsVisible = false;
                });
            });
        }

        private async void HideMenu()
        {
            _butAdd.IsVisible = true;
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    _menuPanel.TranslateTo(_startMenuPosition, 0, 200);
                });
            });
            await Task.Run(() =>
            {
                _butAdd.IsVisible = true;
                Device.BeginInvokeOnMainThread(() =>
                {
                    _butAdd.FadeTo(1, 200);
                });
            });
            await Task.Run(() =>
            {
                Task.Delay(200).Wait();
                Device.BeginInvokeOnMainThread(() =>
                {
                    _menuIsShown = false;
                });
            });
        }
    }
}
