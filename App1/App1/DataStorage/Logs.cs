﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1.DataStorage
{
    public static class Logs
    {
        public const string DebugType = "Debug";
        public const string InfoType = "Info";
        public const string WarningType = "Warning";
        public const string ErrorType = "Error";
        public const string TraceType = "Trace";

        private static GetLogs_ _getLogs;
        private static Action<string, DateTime> _delLogs;
        private static Action<string, string> _writeLogs;

        public static void Init(GetLogs_ func1, Action<string, DateTime> func2, Action<string, string> func3)
        {
            _getLogs = func1;
            _delLogs = func2;
            _writeLogs = func3;
        }
        public static void Debug(string text, params object[] obj)
        {
            if(Commons.Instance.Options.WriteLog)
            {
                _writeLogs(DebugType, string.Format(text, obj));
                Trace(text, obj);
            }
        }

        public static void Info(string text, params object[] obj)
        {
            if (Commons.Instance.Options.WriteLog)
            {
                _writeLogs(InfoType, string.Format(text, obj));
                Trace(text, obj);
            }
        }

        public static void Warning(string text, params object[] obj)
        {
            if (Commons.Instance.Options.WriteLog)
            {
                _writeLogs(WarningType, string.Format(text, obj));
                Trace(text, obj);
            }
        }

        public static void Error(string text, params object[] obj)
        {
            if (Commons.Instance.Options.WriteLog)
            {
                _writeLogs(ErrorType, string.Format(text, obj));
                Trace(text, obj);
            }
        }

        private static void Trace(string text, params object[] obj)
        {
            _writeLogs(TraceType, string.Format(text, obj));
        }

        public static string[] GetAll(string table)
        {
            return _getLogs(table);
        }

        public static void Delete(string table, DateTime to)
        {
            _delLogs(table, to);
        }
    }
}