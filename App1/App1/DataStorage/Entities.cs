﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.DataStorage
{
    public class Record
    {
        public uint Id { get; set; }
        public string CategoryName { get; set; }
        public string IdGoodsServices { get; set; }
        public float Price { get; set; }
        public string Note { get; set; }
        public string Date { get; set; }
    }

    public class GoodsService
    {
        public ushort IdCategory { get; set; }
        public string GsName { get; set; }
    }

    public class Category
    {
        public ushort Id { get; set; }
        public string Name { get; set; }
    }

    public class Options
    {
        public bool UseBudget { get; set; }
        public bool UseSecurity { get; set; }
        public float Budget { get; set; }
        public string Password { get; set; }
        public string CtrlWord { get; set; }
        public string Answer { get; set; }
        public bool ShowResidue { get; set; }
        public bool WriteLog { get; set; }
    }
}
