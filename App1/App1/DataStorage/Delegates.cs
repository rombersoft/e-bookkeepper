﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.DataStorage
{
    public delegate uint ExecuteNonQuery(string sql);
    public delegate object ExecuteScalar(string sql);
    public delegate DataTable Table(string sql);
    public delegate string[] GetLogs_(string table);
}
