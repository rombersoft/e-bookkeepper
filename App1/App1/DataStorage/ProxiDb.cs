﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.DataStorage
{
    public class ProxiDb
    {
        private static ProxiDb _instance;
        private ExecuteNonQuery _executeNonQuery, _insertAndGetId;
        private Action<string> _truncate;
        private Action _clearDb;
        private ExecuteScalar _executeScalar;
        private Table _tableCallback;
        //private Categories _categories;
        //private CategoriesCount _categoriesCount;

        public static ProxiDb Instance
        {
            get
            {
                if (_instance == null) _instance = new ProxiDb();
                return _instance;
            }
        }

        private ProxiDb()
        {
            
        }

        public void SetInsertAndGetId(ExecuteNonQuery insertAndGetId)
        {
            _insertAndGetId = insertAndGetId;
        }

        public void SetExecuteNonQuery(ExecuteNonQuery executeNonQueryWithResult)
        {
            _executeNonQuery = executeNonQueryWithResult;
        }

        public void SetScalarQuery(ExecuteScalar scalarCallback)
        {
            ExecuteNonQuery("VACUUM");
            _executeScalar = scalarCallback;
        }

        public void SetTruncate(Action<string> truncate)
        {
            _truncate = truncate;
        }

        public void SetTableCallback(Table tableCallback)
        {
            _tableCallback = tableCallback;
        }

        public void SetClearDB(Action clearDb)
        {
            _clearDb = clearDb;
        }

        internal uint InsertAndGetId(string sql)
        {
            return _insertAndGetId.Invoke(sql);
        }

        internal uint ExecuteNonQuery(string sql)
        {
            return _executeNonQuery(sql);
        }

        internal object ExecuteScalar(string sql)
        {
            return _executeScalar(sql);
        }

        internal void Truncate(string sql)
        {
            _truncate(sql);
        }

        internal DataTable GetTable(string sql)
        {
            return _tableCallback.Invoke(sql);
        }

        internal void ClearDb()
        {
            _clearDb.Invoke();
        }
    }
}
