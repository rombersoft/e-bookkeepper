﻿using System;
using System.Collections.Generic;
using System.Data;

namespace App1.DataStorage
{
    public class NominationDataStorage
    {
        public bool Store(GoodsService goodService)
        {
            string sql = "INSERT INTO GoodsServices VALUES (" + goodService.IdCategory + ", '" + goodService.GsName.Replace('\'', '`') + "')";
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("Inserted {0} goodsServices", count);
            return count > 0;
        }

        public bool Rename(string oldName, string newName)
        {
            string sql = "UPDATE GoodsServices SET `Name` = '" + newName.Replace('\'', '`') + "' WHERE `Name` = '" + oldName + "'";
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("Updated {0} goodsServices", count);
            return count > 0;
        }

        public GoodsService[] GetAll()
        {
            string sql = "SELECT * FROM GoodsServices ORDER BY `Name`";
            return Load(sql);
        }

        public GoodsService[] GetByCategory(ushort id)
        {
            string sql = "SELECT * FROM GoodsServices WHERE IdCategories = " + id + " ORDER BY `Name`";
            return Load(sql);
        }

        private GoodsService[] Load(string sql)
        {
            DataTable table = ProxiDb.Instance.GetTable(sql);
            List<GoodsService> nomination = new List<GoodsService>();
            foreach (DataRow row in table.Rows)
            {
                nomination.Add(new GoodsService() { IdCategory = Convert.ToUInt16(row[0]), GsName = row[1].ToString() });
            }
            Logs.Info("Gotten {0} goodsServices", nomination.Count);
            return nomination.ToArray();
        }

        public bool Delete(string name)
        {
            string sql = "DELETE FROM GoodsServices WHERE `Name` = '" + name + "'";
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("Deleted {0} goodsServices by name <{1}>", count, name);
            return count > 0;
        }
    }
}
