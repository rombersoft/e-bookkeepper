﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace App1.DataStorage
{
    public class SettingsDataStorage
    {
        public bool Store<T>(T data)
        {
            string action;
            string commandString = String.Format("SELECT Count(*) FROM Settings WHERE `Key` = '{0}'", typeof(T).Name);
            if (Convert.ToInt32(ProxiDb.Instance.ExecuteScalar(commandString)) > 0)
            {
                commandString = String.Format("UPDATE Settings SET Value = '{0}' WHERE `Key` = '{1}'", JsonConvert.SerializeObject(data).Replace('\'', '`'), typeof(T).Name);
                action = "Updated";
            }
            else
            {
                commandString = String.Format("INSERT INTO Settings (`Key`, Value) VALUES('{0}', '{1}')", typeof(T).Name, JsonConvert.SerializeObject(data).Replace('\'', '`'));
                action = "Inserted";
            }
            uint count = ProxiDb.Instance.ExecuteNonQuery(commandString);
            Logs.Info("{0} {1} settings", action, count);
            return count > 0;
        }

        public T Load<T>()
        {
            string[] array = typeof(T).Name.Split('.');
            string ident = array[array.Length - 1];
            string commandString = String.Format("SELECT Value FROM Settings WHERE `Key` = '{0}'", ident);
            object obj = ProxiDb.Instance.ExecuteScalar(commandString);
            string strSetting = null;
            if(obj != null) strSetting = obj.ToString();
            if (string.IsNullOrEmpty(strSetting))
            {
                //Logs.Warning("Object {0} not found into table <Settings>", ident);
                throw new ObjectNotFoundException(String.Format("Object {0} not found into table <Settings>", ident));
            }
            try
            {
                T setting = JsonConvert.DeserializeObject<T>(strSetting);
                //Logs.Info("Settings {0} was gotten successfully", ident);
                return setting;
            }
            catch (Exception)
            {
                //Logs.Error("Error of deserializing {0} from table <Settings>. Exception message: {1}", ident, ex.Message);
                Remove(ident);
                throw new ObjectNotFoundException(String.Format("Error of deserializing {0} from table <Settings>", ident));
            }
        }

        public void Remove(String ident)
        {
            string commandString = String.Format("DELETE FROM Settings WHERE `Key` = '{0}'", ident);
            uint count = ProxiDb.Instance.ExecuteNonQuery(commandString);
            Logs.Info("Settings {0} was deleted {1} times", ident, count);
        }
    }
}