﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace App1.DataStorage
{
    public class CategoryDataStorage
    {
        public uint GetCount()
        {
            string sql = "SELECT Count(*) FROM Categories";
            uint count = Convert.ToUInt32(ProxiDb.Instance.ExecuteScalar(sql));
            Logs.Info("Gotten {0} categories", count);
            return count;
        }

        public bool Store(Category category)
        {
            string sql, action;
            if (category.Id == 0)
            {
                sql = "INSERT INTO Categories (`Name`) VALUES ('" + category.Name.Replace('\'', '`') + "')";
                action = "Inserted";
            }
            else
            {
                sql = "UPDATE Categories SET Name = '" + category.Name.Replace('\'', '`') + "' WHERE Id = " + category.Id;
                action = "Updated";
            }
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("{0} {1} categories", action, count);
            return count > 0;
        }

        public Category[] GetAll()
        {
            string sql = "SELECT * FROM Categories ORDER BY `Name`";
            DataTable table = ProxiDb.Instance.GetTable(sql);
            List<Category> categories = new List<Category>();
            foreach (DataRow row in table.Rows)
            {
                categories.Add(new Category() {Id = Convert.ToUInt16(row[0]), Name = row[1].ToString()});
            }
            Logs.Info("Gotten {0} categories", categories.Count);
            return categories.ToArray();
        }

        public bool Delete(ushort id)
        {
            string sql = "DELETE FROM Categories WHERE Id = " + id;
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("Deleted {0} categories by id <{1}>", count, id);
            return count > 0;
        }
    }
}
