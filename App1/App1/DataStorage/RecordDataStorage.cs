﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.DataStorage
{
    public class RecordDataStorage
    {
        public uint GetCount()
        {
            string sql = "SELECT Count(*) FROM Records";
            uint count = Convert.ToUInt32(ProxiDb.Instance.ExecuteScalar(sql));
            Logs.Info("Gotten count records: {0}", count);
            return count;
        }

        public bool Store(Record record)
        {
            string sql;
            if (!string.IsNullOrEmpty(record.Note)) record.Note = record.Note.Replace('\'', '`');
            string action;
            if (record.Id == 0)
            {
                sql = "INSERT INTO Records (IdGoodsServices, Price, Note, Date) VALUES " +
                    "('" + record.IdGoodsServices + "', " + record.Price.ToString(CultureInfo.InvariantCulture) + ", '" + record.Note + "', '" + record.Date + "')";
                action = "Inserted";
            }
            else
            {
                sql = "UPDATE Records SET IdGoodsServices = '" + record.IdGoodsServices + "', Note = '" + record.Note + "', Price = " + record.Price +
                    " WHERE Id = " + record.Id;
                action = "Updated";
            }
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("{0} {1} records. Request {2}", action, count, sql);
            return count > 0;
        }

        public Record[] GetAll()
        {
            string sql = "SELECT Records.Id, Records.IdGoodsServices, round(Records.Price, 2), Records.Note, Records.Date, Categories.Name FROM Records " +
                "INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name " +
                "INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories ORDER BY Date";
            DataTable table = ProxiDb.Instance.GetTable(sql);
            return FromDataTable(table);
        }

        public Record[] GetForToday()
        {
            string sql = "SELECT Records.Id, Records.IdGoodsServices, round(Records.Price, 2), Records.Note, Records.Date, Categories.Name FROM Records " +
                "INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name " +
                "INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories WHERE Date > '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ORDER BY Date";
            DataTable table = ProxiDb.Instance.GetTable(sql);
            return FromDataTable(table);
        }

        public Record[] GetForCurrentMonth()
        {
            string sql = "SELECT Records.Id, Records.IdGoodsServices, round(Records.Price, 2), Records.Note, Records.Date, Categories.Name FROM Records " +
                "INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name " +
                "INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories WHERE Date > '" + DateTime.Now.ToString("yyyy-MM") + "-01' ORDER BY Date";
            DataTable table = ProxiDb.Instance.GetTable(sql);
            return FromDataTable(table);
        }

        public Record[] GetForPeriod(DateTime from, DateTime to)
        {
            to = to.AddDays(1);
            string sql = "SELECT Records.Id, Records.IdGoodsServices, round(Records.Price, 2), Records.Note, Records.Date, Categories.Name FROM Records " +
                "INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name " +
                "INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories WHERE Date BETWEEN '"
                + from.ToString("yyyy-MM-dd") + "' AND '" + to.ToString("yyyy-MM-dd") + "' ORDER BY Date";
            DataTable table = ProxiDb.Instance.GetTable(sql);
            return FromDataTable(table);
        }

        private Record[] FromDataTable(DataTable table)
        {
            List<Record> records = new List<Record>();
            foreach (DataRow row in table.Rows)
            {
                Record record = new Record()
                {
                    Id = Convert.ToUInt32(row[0]),
                    IdGoodsServices = row[1].ToString(),
                    Price = float.Parse(row[2].ToString(), System.Globalization.NumberStyles.Float),
                    Note = row[3].ToString(),
                    CategoryName = row[5].ToString()
                };
                DateTime dt = DateTime.Parse(row[4].ToString());
                DateTime now = DateTime.Now;
                if (dt.Year == now.Year)
                {
                    if (dt.Month == now.Month)
                    {
                        if (dt.Day == now.Day) record.Date = dt.ToString("yyyy-MM-dd HH:mm:ss").Substring(11);
                        else record.Date = dt.ToString("dd-MMM HH:mm");
                    }
                    else record.Date = dt.ToString("dd-MMM");
                }
                else record.Date = dt.ToString("yyyy-MM-dd");
                records.Add(record);
            }
            Logs.Info("Returned {0} records", records.Count);
            return records.ToArray();
        }

        public float GetAmountForCurrentMonth()
        {
            float amount = 0;
            string currentMonth = DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString();
            string sql = "SELECT Sum(Price) FROM Records WHERE Date LIKE '" + DateTime.Now.Year + "-" + currentMonth + "%' ORDER BY Date Desc";
            object obj = ProxiDb.Instance.ExecuteScalar(sql);
            if (obj != null)
            {
                string str = obj.ToString();
                amount = float.Parse(str, CultureInfo.CurrentUICulture);
            }
            Logs.Info("Gotten amount {0} for current month", amount);
            return amount;
        }

        public Dictionary<string, float> GetStatisticsByCategories(DateTime from, DateTime to)
        {
            string sql = "SELECT Categories.Name, SUM(printf(\"%.2f\", Records.Price)) as Price FROM Records INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name " +
                "INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories WHERE Date BETWEEN '" + from.ToString("yyyy-MM-dd") + "' AND '" + to.ToString("yyyy-MM-dd") +
                "' GROUP BY Categories.Name";
            return StatisticsFromDataTable(sql);
        }

        public Dictionary<string, float> GetStatisticsByCategory(ushort id, DateTime from, DateTime to)
        {
            string sql = "SELECT Records.IdGoodsServices, SUM(printf(\" % .2f\", Records.Price)) as Price FROM Records " +
                "INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories " +
                "WHERE Date BETWEEN '" + from.ToString("yyyy-MM-dd") + "' AND '" + to.ToString("yyyy-MM-dd") + "' AND Categories.Id = " + id + " GROUP BY Records.IdGoodsServices";
            return StatisticsFromDataTable(sql);
        }

        public Dictionary<string, float> GetStatisticsByName(DateTime from, DateTime to)
        {
            string sql = "SELECT Records.IdGoodsServices, SUM(printf(\" % .2f\", Records.Price)) as Price FROM Records INNER JOIN GoodsServices ON Records.IdGoodsServices = GoodsServices.Name " +
                "INNER JOIN Categories ON Categories.Id = GoodsServices.IdCategories WHERE Date BETWEEN '" + from.ToString("yyyy-MM-dd") + "' AND '" + to.ToString("yyyy-MM-dd") +
                "' GROUP BY Records.IdGoodsServices";
            return StatisticsFromDataTable(sql);
        }

        private Dictionary<string, float> StatisticsFromDataTable(string sql)
        {
            Dictionary<string, float> statistics = new Dictionary<string, float>(200);
            DataTable table = ProxiDb.Instance.GetTable(sql);
            foreach(DataRow row in table.Rows)
            {
                statistics.Add(row[0].ToString(), float.Parse(row[1].ToString(), System.Globalization.NumberStyles.Float));
            }
            Logs.Info("Gotten statistics with {0} elements.", statistics.Count);
            return statistics;
        }

        public bool Delete(uint id)
        {
            string sql = "DELETE FROM Records WHERE Id = " + id;
            uint count = ProxiDb.Instance.ExecuteNonQuery(sql);
            Logs.Info("Deleted a {0} records", count);
            return count > 0;
        }
    }
}
