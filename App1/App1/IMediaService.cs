﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public interface IMediaService
    {
        Task OpenGallery();

        Task CreateDbCopy();

        void RestoreDB(byte[] buff);
    }
}