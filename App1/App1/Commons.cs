﻿using App1.DataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Commons
    {
        public Options Options { get; private set; }
        public byte AuthorizedAgo { get; private set; }
        private static Commons _instance;

        public static Commons Instance
        {
            get
            {
                if (_instance == null) _instance = new Commons();
                return _instance;
            }
        }

        private Commons()
        {
            AuthorizedAgo = 11;
            try
            {
                Options = new SettingsDataStorage().Load<Options>();
            }
            catch (ObjectNotFoundException)
            {
                Options = new Options();
            }
        }

        public bool CheckPassword(string password)
        {
            if (password == Options.Password)
            {
                AuthorizedSuccess();
                return true;
            }
            return false;
        }

        public bool CheckKeyWord(string keyWord)
        {
            if(keyWord == Options.Answer)
            {
                AuthorizedSuccess();
                return true;
            }
            return false;
        }

        public void StoreOptions()
        {
            new SettingsDataStorage().Store(Commons.Instance.Options);
            AuthorizedSuccess();
        }

        private async void AuthorizedSuccess()
        {
            AuthorizedAgo = 0;
            await Task.Run(() =>
            {
                while (true)
                {
                    if (++AuthorizedAgo > 10)
                        break;
                    Task.Delay(60000).Wait();
                }
            });
        }
    }
}
