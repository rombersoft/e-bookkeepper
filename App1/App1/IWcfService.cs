﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public interface IWcfService
    {
        Task<uint> SendReport(string application, string ident, string email, string text, byte[] buff);

        Task UploadLogs(string application, string ident, string version, string[] text);
    }
}
