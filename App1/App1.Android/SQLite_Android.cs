﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App1;
using App1.DataStorage;
using App1.Droid;
using Xamarin.Forms;
using System.Diagnostics;
using DbLogic.Droid;

[assembly: Dependency(typeof(SQLite_Android))]
namespace App1.Droid
{
    public class SQLite_Android : ISqlite
    {
        public SQLite_Android() { }

        public string GetDatabasePath(string sqliteFilename)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, sqliteFilename);
            //File.Delete(path);
            // копирование файла из папки Assets по пути path
            //File.Copy(path, Path.Combine(global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, sqliteFilename));
            if (!File.Exists(path))
            {
                Stream dbAssetStream = null;
                try
                {
                    dbAssetStream = MainActivity.Instance.Assets.Open(sqliteFilename);
                }
                catch(Exception ex)
                {
                    if (sqliteFilename == "Logs.db3") Debug.WriteLine(ex.Message);
                    else SQLiteDbProvider.Instance.WriteLog("Warning", ex.Message);
                }

                var dbFileStream = new FileStream(path, System.IO.FileMode.OpenOrCreate);
                var buffer = new byte[1024];

                int b = buffer.Length;
                int length;

                while ((length = dbAssetStream.Read(buffer, 0, b)) > 0)
                {
                    dbFileStream.Write(buffer, 0, length);
                }

                dbFileStream.Flush();
                dbFileStream.Close();
                dbFileStream.Dispose();
                dbAssetStream.Close();
            }
            return path;
        }

        private void ForeachFolder(string folder)
        {
            foreach (string file in Directory.GetFiles(folder))
            {
                Debug.WriteLine(file);
            }
            foreach(string folder1 in Directory.GetDirectories(folder))
            {
                ForeachFolder(folder1);
            }
        }
    }
}