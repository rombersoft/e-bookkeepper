﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.CurrentActivity;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(App1.Droid.MediaService))]
namespace App1.Droid
{
    public class MediaService : Java.Lang.Object, IMediaService
    {
        public async Task OpenGallery()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Plugin.Permissions.Abstractions.Permission.Storage))
                    {
                        Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.needStoragePermissions, ToastLength.Long).Show();
                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Plugin.Permissions.Abstractions.Permission.Storage });
                    status = results[Permission.Storage];
                }

                if (status == PermissionStatus.Granted)
                {
                    var imageIntent = new Intent(Intent.ActionPick);
                    imageIntent.SetType("image/*");
                    imageIntent.PutExtra(Intent.ExtraAllowMultiple, false);
                    imageIntent.SetAction(Intent.ActionGetContent);
                    ((Activity)CrossCurrentActivity.Current.Activity).StartActivityForResult(Intent.CreateChooser(imageIntent, App1.Resx.Resource.selectImg), 0);
                }
                else if (status != PermissionStatus.Unknown)
                {
                    Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.permissionDenied, ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.errorTryAgain, ToastLength.Long).Show();
            }
        }

        public async Task CreateDbCopy()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Plugin.Permissions.Abstractions.Permission.Storage))
                    {
                        Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.needStoragePermissions, ToastLength.Long).Show();
                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Plugin.Permissions.Abstractions.Permission.Storage });
                    status = results[Permission.Storage];
                }

                if (status == PermissionStatus.Granted)
                {
                    string localFilename = "E-Bookkeepper.db3";
                    string sourceFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    var fullSourceFile = Path.Combine(sourceFolder, localFilename);
                    string destFolder = global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/E-Bookkepper";
                    if (!Directory.Exists(destFolder)) Directory.CreateDirectory(destFolder);
                    string fullDestFile = Path.Combine(global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/E-Bookkepper", localFilename);

                    var buffer = new byte[1024];
                    int b = buffer.Length;
                    int length;
                    using (var destStream = new FileStream(fullDestFile, FileMode.OpenOrCreate))
                    {
                        using (FileStream sourceStream = new FileStream(fullSourceFile, FileMode.Open))
                        {
                            while ((length = sourceStream.Read(buffer, 0, b)) > 0)
                            {
                                destStream.Write(buffer, 0, length);
                            }
                        }
                        destStream.Flush();
                    }
                    Toast.MakeText(CrossCurrentActivity.Current.Activity, Resx.Resource.fileCopied + fullDestFile, ToastLength.Long).Show();
                }
                else if (status != PermissionStatus.Unknown)
                {
                    Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.permissionDenied, ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.errorTryAgain, ToastLength.Long).Show();
            }
        }

        public void RestoreDB(byte[] buff)
        {
            try
            {
                string localFilename = "E-Bookkeepper.db3";
                string destFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                var fullDestFile = Path.Combine(destFolder, localFilename);

                using (var destStream = new FileStream(fullDestFile, FileMode.Create))
                {
                    destStream.Write(buff, 0, buff.Length);
                    destStream.Flush();
                }
                Toast.MakeText(CrossCurrentActivity.Current.Activity, Resx.Resource.message29, ToastLength.Long).Show();
            }
            catch(Exception ex)
            {
                Toast.MakeText(CrossCurrentActivity.Current.Activity, ex.Message, ToastLength.Long).Show();
            }
        }
    }
}