﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Net;
using Android.Runtime;
using App1.DataStorage;
using DbLogic.Droid;
using Android.Widget;
using Android.Provider;
using Android.Database;
using Xamarin.Forms;
using Plugin.CurrentActivity;
using System.Linq;

namespace App1.Droid
{
    [Activity(Label = "E-Bookkeepper", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation/*, ScreenOrientation = ScreenOrientation.Landscape | ScreenOrientation.Portrait*/)]
    /*[IntentFilter(new[] { Intent.ActionView }, Categories = new[]
    {
        Intent.CategoryDefault, Intent.CategoryBrowsable
    },
        DataScheme = "file", DataMimeType ="image/*", DataPathPattern = ".*\\.md")]*/
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            Instance = this;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
            AndroidEnvironment.UnhandledExceptionRaiser += AndroidEnvironmentOnUnhandledException;
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            ProxiDb.Instance.SetExecuteNonQuery(SQLiteDbProvider.Instance.ExecuteNonQuery);
            ProxiDb.Instance.SetTableCallback(SQLiteDbProvider.Instance.GetTable);
            ProxiDb.Instance.SetInsertAndGetId(SQLiteDbProvider.Instance.InsertAndGetId);
            ProxiDb.Instance.SetClearDB(SQLiteDbProvider.Instance.ClearDB);
            ProxiDb.Instance.SetScalarQuery(SQLiteDbProvider.Instance.ScalarQuery);
            Logs.Init(SQLiteDbProvider.Instance.GetLogs, SQLiteDbProvider.Instance.DeleteLogs, SQLiteDbProvider.Instance.WriteLog);
            try
            {
                LoadApplication(new App());
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok && requestCode == 0)
            {
                if (data != null)
                {
                    ClipData clipData = data.ClipData;
                    Android.Net.Uri uri = data.Data;
                    var path = GetRealPathFromURI(uri);
                    if (path != null)
                    {
                        if(path.EndsWith(".db3"))
                        {
                            string localFilename = "E-Bookkeepper.db3";
                            string destFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                            var fullDestFile = Path.Combine(destFolder, localFilename);
                            
                            var buffer = new byte[1024];
                            int b = buffer.Length;
                            int length;
                            using (var destStream = new FileStream(fullDestFile, FileMode.OpenOrCreate))
                            {
                                using (FileStream sourceStream = new FileStream(path, FileMode.Open))
                                {
                                    while ((length = sourceStream.Read(buffer, 0, b)) > 0)
                                    {
                                        destStream.Write(buffer, 0, length);
                                    }
                                }
                                destStream.Flush();
                            }
                            Toast.MakeText(CrossCurrentActivity.Current.Activity, Resx.Resource.message29, ToastLength.Long).Show();
                        }
                        else
                        {
                            var image = ImageHelpers.ScalingImage(path);
                            App1.BugReport.SetFileData(image, path.Split('/').Last());
                        }
                    }
                }
            }
        }

        public String GetRealPathFromURI(Android.Net.Uri contentURI)
        {
            ICursor cursor = null;
            try
            {
                string fullPathToImage = "";
                cursor = ContentResolver.Query(contentURI, null, null, null, null);
                cursor.MoveToFirst();
                int idx = cursor.GetColumnIndex(MediaStore.Images.ImageColumns.Data);
                if (idx != -1)
                {
                    fullPathToImage = cursor.GetString(idx);
                }
                else
                {
                    cursor.Close();
                    cursor = null;
                    var docID = DocumentsContract.GetDocumentId(contentURI);
                    var id = docID.Split(':')[1];
                    var whereSelect = MediaStore.Files.FileColumns.Id + "=?";
                    var projections = new string[] { MediaStore.Images.ImageColumns.Data };
                    cursor = ContentResolver.Query(MediaStore.Images.Media.InternalContentUri, projections, whereSelect, new string[] { id }, null);
                    if (cursor.Count == 0)
                    {
                        cursor.Close();
                        cursor = ContentResolver.Query(MediaStore.Images.Media.ExternalContentUri, projections, whereSelect, new string[] { id }, null);
                    }
                    var colData = cursor.GetColumnIndexOrThrow(MediaStore.Images.ImageColumns.Data);
                    cursor.MoveToFirst();
                    fullPathToImage = cursor.GetString(colData);
                }
                return fullPathToImage;
            }
            catch (Exception ex)
            {
                Toast.MakeText(CrossCurrentActivity.Current.Activity, App1.Resx.Resource.unableToGetPath, ToastLength.Long).Show();
            }
            finally
            {
                cursor.Dispose();
            }
            return null;
        }

        private void AndroidEnvironmentOnUnhandledException(object sender, RaiseThrowableEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Exception.ToString());
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Exception.ToString());
        }

        internal static MainActivity Instance { get; private set; }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.ToString());
        }
    }
}

