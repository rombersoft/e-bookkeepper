﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WcfService
{
    public class MyServiceClient : ClientBase<IService>, IService
    {
        public MyServiceClient(Binding binding, EndpointAddress address) : base(binding, address)
        {
        }

        public uint AddBugReport(string ident, string application, string email, string text, byte[] buff)
        {
            return Channel.AddBugReport(ident, application, email, text, buff);
        }

        public void UploadLogs(string application, string ident, string version, string[] text)
        {
            Channel.UploadLogs(application, ident, version, text);
        }
    }
}