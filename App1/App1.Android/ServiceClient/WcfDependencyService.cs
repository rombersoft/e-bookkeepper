﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DbLogic.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(WcfService.WcfDependencyService))]
namespace WcfService
{
    public class WcfDependencyService : App1.IWcfService
    {
        public Task<uint> SendReport(string ident, string application, string email, string text, byte[] buff)
        {
            MyServiceClient client = CreateClient();
            return Task.Run(()=>
            {
                uint result = 2;
                try
                {
                    result = client.AddBugReport(ident, application, email, text, buff);
                }
                catch (Exception ex)
                {
                    SQLiteDbProvider.Instance.WriteLog("Error uploading Logs {0}", ex.ToString());
                }
                return result;
            });
        }

        public Task UploadLogs(string application, string ident, string version, string[] text)
        {
            MyServiceClient client = CreateClient();
            return Task.Run(() =>
            {
                try
                {
                    client.UploadLogs(application, ident, version, text);
                }
                catch (Exception ex)
                {
                    SQLiteDbProvider.Instance.WriteLog("Error uploading Logs {0}", ex.ToString());
                }
            });
        }

        private MyServiceClient CreateClient()
        {
            var binding = new BasicHttpBinding();
            var address = new EndpointAddress("http://rombersoft.dp.ua:1025");
            return new MyServiceClient(binding, address);
        }
    }
}