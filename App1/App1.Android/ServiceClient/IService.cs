﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WcfService
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        uint AddBugReport(string application, string ident, string email, string text, byte[] buff);

        [OperationContract]
        void UploadLogs(string application, string ident, string version, string[] text);
    }
}